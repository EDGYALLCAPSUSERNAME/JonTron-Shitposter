#!/usr/bin/env python3

import re
import os  # for deleting the video and image upon completion
import time
import praw
import json
import random
import string
import base64
import inspect
import logging
import requests
import subprocess  # for starting the ffmpeg process
import OAuth2Util
from Markov import Markov
from pytube import YouTube
from base64 import b64encode
from apiclient.discovery import build

# configure the logger
logging.basicConfig(filename='jontronshitpost.log',
                    level=logging.INFO,
                    format='%(asctime)s %(message)s')

# User config
# --------------------------------------------------------------------
# Google developer information
DEVELOPER_KEY = ''
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'

# Imgur developer information
IMGUR_CLIENT_ID = ''

# Don't include the /r/
SUBREDDIT_NAME = 'jontron'

# Add words to this list that you don't want to show up in any of the
# generated reddit post titles
NO_NO_WORDS = ['[serious]', 'nokel']

# --------------------------------------------------------------------


# import my developer settings
try:
    import bot
    SUBREDDIT_NAME = bot.SUBREDDIT_NAME
    DEVELOPER_KEY = bot.DEVELOPER_KEY
    IMGUR_CLIENT_ID = bot.IMGUR_CLIENT_ID
except ImportError:
    pass


def youtube_search():
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                    developerKey=DEVELOPER_KEY)

    search_response = youtube.search().list(
                        channelId='UCdJdEguB1F1CiYe7OEi3SBg',
                        order='date',
                        part='id, snippet',
                        maxResults=50,
                        type='video').execute()

    videos = []
    for search_result in search_response.get('items', []):
        videos.append(search_result['id']['videoId'])

    return videos


def download_youtube_video(v_id):
    link = 'https://www.youtube.com/watch?v={}'.format(v_id)

    yt = YouTube(link)
    # set the filename
    yt.set_filename('JonTronVideo')

    print('Downloading video...')
    yt.filter('mp4')[-1].download('.')


def get_video_length():
    print('Getting video length...')
    # start a ffprobe process
    result = subprocess.Popen(['ffprobe', 'JonTronVideo.mp4'],
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    # convert the time to seconds
    ts = [x for x in result.stdout.readlines() if b'Duration' in x][0][12:20]
    v_length = ((int(ts[0:2]) * 60) * 60) + int(ts[3:5]) * 60 + int(ts[6:])
    result.stdout.close()
    return v_length


def get_random_frame():
    # get the video length
    v_length = get_video_length()
    frame_time = random.randrange(0, v_length + 1)

    # build the ffmpeg args
    ffmpeg_args = ['ffmpeg', '-ss', str(frame_time), 
                   '-i', 'JonTronVideo.mp4', '-f', 'image2', 'JonFrame.png']

    output = open('ffmpeg_log.txt', 'w')

    print('Creating video frame image...')
    try:
        subprocess.call(ffmpeg_args, stderr=output, stdout=subprocess.PIPE)
    except OSError:  # this will occur if FFmpeg is not installed
        print('ERROR: Could not start FFmpeg process.\
               Please make sure you have FFmpeg installed before running.')
        exit(1)

    output.close()  # close the file
    

def generate_title():
    print('Generating title...')
    with open('JonTronTitles.txt', 'r') as f:
        markov = Markov(f)  # create a markov object
        words = random.randrange(4, 26)  # generate a random title size
        title = markov.generateMarkovText(size=words)  # generate a title

        # remove any instance of words we don't want in titles
        title = re.sub(r'|'.join(map(re.escape, NO_NO_WORDS)), '', title)
        # remove any extra spaces left by word removal
        title = re.sub(' +', ' ', title).strip()

    return string.capwords(title)


def upload_image_to_imgur(title):
    headers = {'Authorization': 'Client-ID {}'.format(IMGUR_CLIENT_ID)}
    url = 'https://api.imgur.com/3/image'
    f = open('JonFrame.png', 'rb')
    print('Uploading to imgur...')
    resp = requests.post(url,
                         headers=headers,
                         data={
                            'image': b64encode(f.read()),
                            'type': 'base64',
                            'name': 'JonFrame.png',
                            'title': title
                         })

    f.close()
    # raise an exception for a bad status to get logged and skipped
    resp.raise_for_status()
    return resp.json()['data']['link']


def upload_to_reddit(r, title, link):
    print('Uploading to reddit...')
    r.submit(SUBREDDIT_NAME, title, url=link, captcha=None)


def cleanup():
    to_remove = ['JonFrame.png', 'JonTronVideo.mp4']
    print('Cleaning up...')

    for f in to_remove:
        os.remove(f)


def format_var_str(dic):
    s = ''
    for var, val in dic.items():
        s += ('\t' * 8) + '{}: {}\n'.format(var, val)

    return s


def main():
    r = praw.Reddit(user_agent='Jon Tron Poster v1.0 /u/cutety')
    o = OAuth2Util.OAuth2Util(r, print_log=True)
    o.refresh(force=True)

    try:
        videos = youtube_search()  # get a list of jontron videos
        download_youtube_video(random.choice(videos))  # download a random one
        get_random_frame()  # extract a random frame
        title = generate_title() # generate a title
        link = upload_image_to_imgur(title)  # upload to imgur
        upload_to_reddit(r, title, link)  # upload to reddit
        cleanup()  # cleanup the files
    except Exception as e:
        print('ERROR: {}'.format(e))

        # log the error event with specific information from the traceback
        traceback_log = '''
        ERROR: {e}
        File "{fname}", line {lineno}, in {fn}
        Time of error: {t}

        Variable dump:

        {g_vars}
        {l_vars}
        '''
        # grabs the traceback info
        frame, fname, lineno, fn = inspect.trace()[-1][:-2]
        # dump the variables and get formated strings
        g_vars = 'Globals:\n' + format_var_str(frame.f_globals)
        l_vars = 'Locals:\n' + format_var_str(frame.f_locals)

        logging.debug(traceback_log.format(e=e, lineno=lineno, fn=fn,
                                           fname=fname, t=time.strftime('%c'),
                                           g_vars=g_vars, l_vars=l_vars))


if __name__ == '__main__':
    if not SUBREDDIT_NAME:
        print('Subreddit name has not been set.\nExiting...')
        exit(1)

    main()