Jon Tron Shit Poster
====================

Installations
-------------

This script requires python 3, praw, praw-oauth2util, pytube, and the google api.

To install these type in your command line:

    pip install praw
    pip install praw-oauth2util
    pip install pytube
    pip install google-api-python-client

This script also requires FFMpeg to be installed. FFmpeg can be downloaded [here](https://www.ffmpeg.org/download.html). Or you can install it with your favorite package manager:

    sudo apt-get install ffmpeg
    brew install ffmpeg

*Note: If you are having troubles getting it to run on Windows because of FFMpeg install issues [see this wiki page](https://github.com/TheNickHurst/MovieBarcodeCreator/wiki/Windows-FFMpeg-Install) for a guide to setting it up*

Reddit OAuth Setup
------------------

* [Go here on the account you want the bot to run on](https://www.reddit.com/prefs/apps/)
* Click on create a new app.
* Give it a name. 
* Select script from the radio buttons.
* Set the redirect uri to `http://127.0.0.1:65010/authorize_callback`
* After you create it you will be able to access the app key and secret.
* The **app key** is found here. Add this to the app_key variable in the oauth.ini file.

![here](http://i.imgur.com/7ybI5Fo.png) (**Do not give out to anybody**) 

* And the **app secret** here. Add this to the app_secret variable in the oauth.ini file.

![here](http://i.imgur.com/KkPE3EV.png) (**Do not give out to anybody**)

Note: On the first run of the bot you'll need to run it on a system that has a desktop environment.
So, if you're planning on running the bot on a server or VPS somewhere, run it on your computer
first. The first run will require you to authenticate it with the associated Reddit account by
opening a web page that looks like this:

![authentication page](http://i.imgur.com/se53uTq.png)

It will list different things it's allowed to do depending on the bots scope. After you
authenticate it, that page won't pop up again unless you change the OAuth credentials. And you'll
be free to run it on whatever environment you choose.

Setting Up Your Google Dev Account
----------------------------------

Then you must get your developer key from google, to do that go to:

[console.developers.google.com/project](https://console.developers.google.com/project)

And create a project.

From the console now, go to APIs & auth, and navigate to the APIs screen.

Click on YouTube Data API

![Youtube data api](http://i.imgur.com/jN79VWq.png)

And click enable API at the top.

![Activate Youtube API](http://i.imgur.com/9LvPHgQ.png)

From there click on credentials and you'll see in the Public API Access area
and line called API key, copy that into the variable DEVELOPER_KEY in
youtube_submitter_bot.py.

![Developer key](http://i.imgur.com/MR9recA.png)

Imgur Developer Setup
----------------------

* Go [here](https://api.imgur.com/oauth2/addclient).
* Fill out the information in the form.
    - Set the Authorization Callback URL to: `http://127.0.0.1:65010/authorize_callback`
* Once the form is completed copy the CLIENT_ID given to you on the following page into the IMGUR_CLIENT_ID variable in jon_tron_shit_poster.py.

Notes
-----

The JonTronTitles.txt is what is used to generate the post titles.

License
-------

The MIT License (MIT)

Copyright (c) 2015 Nick Hurst

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
