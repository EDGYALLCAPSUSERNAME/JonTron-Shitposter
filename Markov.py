import random

class Markov:

    def __init__(self, infile):
        self.cache = {}
        self.infile = infile
        self.words = self.fileToWords()
        self.word_size = len(self.words)
        self.database()


    def fileToWords(self):
        self.infile.seek(0)
        data = self.infile.read()
        words = data.split()
        return words


    def triples(self):
        if len(self.words) < 3:
            return

        for i in range(len(self.words) - 2):
            yield(self.words[i], self.words[i+1], self.words[i+2])


    def database(self):
        for w1, w2, w3 in self.triples():
            key = (w1, w2)
            if key in self.cache:
                self.cache[key].append(w3)
            else:
                self.cache[key] = [w3]


    def generateMarkovText(self, size=25):
        seed = random.randint(0, self.word_size - 3)
        seed_word, next_word = self.words[seed], self.words[seed+1]
        w1, w2 = seed_word, next_word
        gen_words = []
        for i in range(size):
            gen_words.append(w1)
            w1, w2 = w2, random.choice(self.cache[(w1, w2)])
        gen_words.append(w2)

        return ' '.join(gen_words)